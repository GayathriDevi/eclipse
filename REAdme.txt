Importing Projects from BitBucket 
==================================


Steps to be done:
-----------------

1) Choose File Menu

2) Select Import option, you will see a window to Import one or more projects from a Git Repository.

3) Drop down Git folder and choose Pr0jects from Git

4) Enter Next

5) Window prompting for Selecting a location of Git Repositories , Choose Clone URI and click Next

6) Enter the location of the source repository as below
  
        a) URI  		:  https://GayathriDevi@bitbucket.org/GayathriDevi/spark.git
        b) Host 		:  bitbucket.org  
	c) Repository path 	:  /GayathriDevi/spark.git
        d) Authentication	:
			User    :  GayathriDevi
                       Password :  *********

7) Click Next

8) Window prompts for password, provide it

9) Choose master for Branch Selection, click Next

10) Enter the destination path /home/roonish/git/spark

11) Enter Next and provide password 

12) Click Finish






   

 



